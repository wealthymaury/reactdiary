/*
 * Cacheo de plugins 
 */

var gulp            = require('gulp'),
	stylus          = require('gulp-stylus'),
	nib             = require('nib'),
	minifyCSS       = require('gulp-minify-css'),
	browserify      = require('browserify'),
	reactPreset     = require('babel-preset-react'),
	es2015Preset    = require('babel-preset-es2015'),
	babelify        = require('babelify'),
	source          = require('vinyl-source-stream'),
	buffer          = require('vinyl-buffer'),
	uglify          = require('gulp-uglify'),
	rename          = require('gulp-rename');

/*
 * Configuracion de directorios
 */

var config = {
	styles: {
		entry: './static/src/stylus/master.styl',
		watch: './static/src/stylus/**/*.styl',
		output: './static/dist/css'
	},
	scripts: {
		entry: './static/src/scripts/app.js',
		watch: './static/src/scripts/**/*.js',
		output: './static/dist/js'
	}
};

// Tarea para compilar stylus
gulp.task('build:css', function() {
	gulp.src(config.styles.entry)
		.pipe(stylus({
			use: nib(),
			'include css': true
		}))
		.pipe(minifyCSS())
		.pipe(rename('styles.min.css'))
		.pipe(gulp.dest(config.styles.output));
});

// Tarea para browserify, minificando el JS
gulp.task('build:js', function() {
	return browserify({ 
			entries: config.scripts.entry, 
			debug: true, 
			transform: [ 
				babelify.configure({
					'presets': [ reactPreset, es2015Preset ]
				})
			]
		})
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(buffer())
		//.pipe(uglify()) //con esto lo minificamos
		.pipe(rename('app.min.js'))
		.pipe(gulp.dest(config.scripts.output));
});

// Tarea para hacer watch de los cambios
gulp.task('watch', function() {
	gulp.watch(config.styles.watch, ['build:css']);
	gulp.watch(config.scripts.watch, ['build:js']);
});

// Accesos a tareas
gulp.task('build', ['build:css', 'build:js'])
gulp.task('default', ['watch', 'build'])

