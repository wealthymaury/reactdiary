import React, { Component } from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import ClientList from '../views/client-list';
import { fetchClients, fetchClientsSuccess, fetchClientsFailure, deleteClient, deleteClientSuccess, deleteClientFailure } from '../../actions/client-actions';
import * as clientApi from '../../api/client-api'

class ClientListContainer extends Component {

	componentDidMount() {
		store.dispatch(fetchClients());

		clientApi.getClients()
			.then(response => {
				store.dispatch(fetchClientsSuccess(response.data));
			})
			.catch(error => {
				store.dispatch(fetchClientsFailure(error.data.detail));
			});
	}

	deleteClient(id) {
		store.dispatch(deleteClient());
		
		clientApi.deleteClient(id)
			.then(response => {
				store.dispatch(deleteClientSuccess(id));
			})
			.catch(error => {
				store.dispatch(deleteClientFailure(error.data.detail));
			});
	}
	
	render() {
		return (
			<ClientList clientsList={this.props.clientsList} deleteClient={this.deleteClient} deletedClient={this.props.deletedClient} />
		);
	}
}

const mapStateToProps = function(store) {
	return {
		clientsList: store.clientState.clientsList,
		deletedClient: store.clientState.deletedClient
	};
};

export default connect(mapStateToProps)(ClientListContainer);