import React, { Component } from 'react';
import { connect } from 'react-redux';
import store from '../../store';
import ClientDetail from '../views/client-detail';
import { fetchClient, fetchClientSuccess, fetchClientFailure, resetActivePost } from '../../actions/client-actions';
import * as clientApi from '../../api/client-api'

class ClientDetailContainer extends Component {

	componentWillUnmount() {
		store.dispatch(resetActivePost());
	}

	componentDidMount() {
		store.dispatch(fetchClient());
		
		clientApi.getClient(this.props.params.clientId)
			.then(response => {
				store.dispatch(fetchClientSuccess(response.data));
			})
			.catch(error => {
				store.dispatch(fetchClientFailure(error.detail));
			});
	}
	
	render() {
		return (
			<ClientDetail activeClient={this.props.activeClient} />
		);
	}
}

const mapStateToProps = function(store) {
	return {
		activeClient: store.clientState.activeClient
	};
};

export default connect(mapStateToProps)(ClientDetailContainer);