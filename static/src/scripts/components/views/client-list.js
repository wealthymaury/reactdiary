import React, { Component } from 'react';
import { Link } from 'react-router';
import uid from 'uid';

class ClientList extends Component {

	renderClients(clients) {
		if (clients.length == 0) {
			return this.renderEmpty();
		}
		
		return clients.map((client) => {
			return (
				<li className="collection-item avatar fix-padding-left" key={ client.id }>
					<img src="#" alt="Usuario" className="circle" />
					<span className="title">{ client.first_name }</span>
					<p>{ client.last_name }</p>

					<div href="#!" className="secondary-content">
						<i className="material-icons icon-mini small grey-text">edit</i>
						<i className="material-icons icon-mini small grey-text" onClick={ this.props.deleteClient.bind(null, client.id) }>delete</i>

						<Link to={ `/clients/${client.id}` }>
							<i className="material-icons icon-mini small grey-text">send</i>
						</Link>
					</div>
				</li>
			);
		});
	}

	renderEmpty() {
		return (
			<li className="collection-item avatar fix-padding-left">
				<i className="material-icons circle teal">clear</i>
				<span className="title">No hay clientes registrados</span>
			</li>
		);
	}

	renderLoading() {
		return (
			<li className="collection-item">
				<h5 className="center-align">Cargando...</h5>
				<div className="progress">
					<div className="indeterminate"></div>
				</div>
			</li>
		);
	}

	renderError(...errors) {
		return (
			<li className="collection-item avatar fix-padding-left">
				<i className="material-icons circle teal">clear</i>
				{
					errors.map(error => {
						return <span className="title" key={ uid() }>{ error }</span>
					})
				}
			</li>
		);
	}

	render() {
		const { clients, loading, error } = this.props.clientsList;
		const deletedClient = this.props.deletedClient;
		
		if(loading) {
			return this.renderLoading();
		} else if(error || deletedClient.error) {
			return this.renderError(error, deletedClient.error);
		}

		return (
			<ul className="collection with-header border-none">
				<li className="collection-header"><h5>Clientes</h5></li>
				<div className="content">
					{ this.renderClients(clients) }
				</div>
			</ul>
		);
	}
}

export default ClientList;