import React, { Component } from 'react';

class ClientDetail extends Component {

	renderClient(client) {
		return (
			<div className="container">
				<h3>{ client.first_name }{ client.last_name }</h3>
			</div>
		);
	}

	renderEmpty() {
		return (
			<div className="container">
				El cliente seleccionado no existe...
			</div>
		);
	}

	renderLoading() {
		return (
			<li className="collection-item">
				<h5 className="center-align">Cargando...</h5>
				<div className="progress">
					<div className="indeterminate"></div>
				</div>
			</li>
		);
	}

	renderError(error) {
		return (
			<li className="collection-item avatar fix-padding-left">
				<i className="material-icons circle teal">clear</i>
				<span className="title">{ error }</span>
			</li>
		);
	}

	render() {
		const { client, loading, error } = this.props.activeClient;

		if (loading) {
			return this.renderLoading();
		} else if(error) {
			return this.renderError(error);
		} else if(!client) {
			return this.renderEmpty();
		} 

		return this.renderClient(client);
	}

}

export default ClientDetail;