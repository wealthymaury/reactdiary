import React, {Component} from 'react';
import {Link} from 'react-router';

class MainLayout extends Component {
	render() {
		return (
			<div className="app">
				<nav className="teal nav-bottom">
					<a href="#" className="js-list brand">BACKDIARY</a>
					<ul className="right">
						<li><Link to="/" activeClassName="active"><i className="material-icons small">add</i></Link></li>
						<li><Link to="/clients" activeClassName="active"><i className="material-icons small">subject</i></Link></li>
						<li><Link to="/" activeClassName="active"><i className="material-icons small">refresh</i></Link></li>
						<li><Link to="/" activeClassName="active"><i className="material-icons small">clear</i></Link></li>
					</ul>
				</nav>
				<div className="container main row">
					<div className="col s12 white z-depth-1">
						<main>
							{this.props.children}
						</main>
					</div>
				</div>
			</div>
		);
	}
}

export default MainLayout;