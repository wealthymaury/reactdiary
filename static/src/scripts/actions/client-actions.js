import * as types from './action-types';

/*
 * LIST
 * */

export function fetchClients() {
	return {
		type: types.FETCH_CLIENTS,
		payload: []
	};
}

export function fetchClientsSuccess(clients) {
	return {
		type: types.FETCH_CLIENTS_SUCCESS,
		payload: clients
	};
}

export function fetchClientsFailure(error) {
	return {
		type: types.FETCH_CLIENTS_FAILURE,
		payload: error
	};
}

/*
 * DETAIL
 * */

export function fetchClient(id) {
	return {
		type: types.FETCH_CLIENT
	};
}

export function fetchClientSuccess(activeClient) {
	return {
		type: types.FETCH_CLIENT_SUCCESS,
		payload: activeClient
	}
}

export function fetchClientFailure(error) {
	return {
		type: types.FETCH_CLIENT_FAILURE,
		payload: error
	}
}

export function resetActivePost() {
	return {
		type: types.RESET_ACTIVE_CLIENT
	}
}

/*
 * DELETE
 * */

export function deleteClient(id) {
	return {
		type: types.DELETE_CLIENT
	}
}

export function deleteClientSuccess(deletedClient) {
	return {
		type: types.DELETE_CLIENT_SUCCESS,
		payload: deletedClient
	}
}

export function deleteClientFailure(response) {
	return {
		type: types.DELETE_CLIENT_FAILURE,
		payload: response
	}
}

/*
 * CREATE
 * */

export function createClient(data) {
	return {
		type: types.CREATE_CLIENT,
		payload: data
	}
}

export function createClientSuccess(newClient) {
	return {
		type: types.CREATE_CLIENT_SUCCESS,
		payload: newClient
	}
}

export function createClientFailure(error) {
	return {
		type: types.CREATE_CLIENT_FAILURE,
		payload: error
	}
}
