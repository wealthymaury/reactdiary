import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

import Home from './components/home';

import MainLayout from './components/layouts/main-layout';

import ClientListContainer from  './components/containers/client-list-container';
import ClientDetailContainer from './components/containers/client-detail-container';

export default (
	<Router history={browserHistory}>
		<Route component={MainLayout}>
			<Route path="/" component={Home} />
			<Route path="clients">
				<IndexRoute component={ClientListContainer} />
				<Route path=":clientId" component={ClientDetailContainer} />
			</Route>
		</Route>
	</Router>
);
