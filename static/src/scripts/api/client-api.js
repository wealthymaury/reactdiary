import axios from 'axios';
import store from '../store';
import { deleteClientSuccess } from '../actions/client-actions';
import { getCSRFToken } from '../utilities'

const ROOT_URL = '/api/clients';

let clientApi = axios.create({
	headers: {
		'csrftoken': 'foobar',
		'X-CSRFToken': getCSRFToken(),
	}
});

export function getClients() {
	return clientApi.get(`${ROOT_URL}/`); 
}

export function getClient(id) {
	return clientApi.get(`${ROOT_URL}/${id}/`);
}

export function deleteClient(id) {
	return clientApi.delete(`${ROOT_URL}/${id}/`);
}

export function createClient(data) {
	return clientApi.post(`${ROOT_URL}/`, data);
}
