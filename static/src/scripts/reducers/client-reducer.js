import * as types from '../actions/action-types';

const INITIAL_STATE = {
	clientsList: { clients: [], error: null, loading: false },
	newClient: { client: null, error: null, loading: false },
	activeClient: { client: null, error: null, loading: false },
	deletedClient: { client: null, error: null, loading: false },
};

const clientReducer = function(state = INITIAL_STATE, action) {
	switch(action.type) {
		// LIST
		case types.FETCH_CLIENTS:
			return Object.assign({}, state, { clientsList: { clients: [], error: null, loading: true }});

		case types.FETCH_CLIENTS_SUCCESS:
			return Object.assign({}, state, { clientsList: { clients: action.payload, error: null, loading: false }});

		case types.FETCH_CLIENTS_FAILURE:
			return Object.assign({}, state, { clientsList: { clients: [], error: action.payload, loading: false }});			

		case types.RESET_CLIENTS:
			return Object.assign({}, state, { clientsList: { clients: [], error: null, loading: false }});

		// DETAIL
		case types.FETCH_CLIENT:
			return Object.assign({}, state, { activeClient: { client: null, error: null, loading: true }});

		case types.FETCH_CLIENT_SUCCESS:
			return Object.assign({}, state, { activeClient: { client: action.payload, error: null, loading: false }});

		case types.FETCH_CLIENT_FAILURE:
			return Object.assign({}, state, { activeClient: { client: null, error: action.payload, loading: false }});

		case types.RESET_ACTIVE_CLIENT:
			return Object.assign({}, state, { activeClient: { client: null, error: null, loading: false }});

		//DELETE
		case types.DELETE_CLIENT:
			return Object.assign({}, state, { deletedClient: { client: null, error: null, loading: true }});
			
		case types.DELETE_CLIENT_SUCCESS:
			let newClients = state.clientsList.clients.filter(client => { 
				return client.id !== action.payload;
			});
			
			return Object.assign({}, state, {
				clientsList: { clients: newClients, error: null, loading: false },
				deletedClient: { client: action.payload, error: null, loading: false }
			});
		
		case types.DELETE_CLIENT_FAILURE:
			return Object.assign({}, state, { deletedClient: { client: null, error: action.payload, loading: false }});

		case types.RESET_DELETED_CLIENT:
			return Object.assign({}, state, { deletedClient: { client: null, error: null, loading: false }});

		//CREATE
		case types.CREATE_CLIENT:
			return Object.assign({}, state, { newClient: { client: null, error: null, loading: true }});

		case types.CREATE_CLIENT_SUCCESS:
			return Object.assign({}, state, { newClient: { client: action.payload, error: null, loading: false }});

		case types.CREATE_CLIENT_FAILURE:
			return Object.assign({}, state, { newClient: { client: null, error: action.payload, loading: false }});

		case types.RESET_NEW_CLIENT:
			return Object.assign({}, state, { newClient: { client: null, error: null, loading: false }});
	}

	return state;
}

export default clientReducer;
