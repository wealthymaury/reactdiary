import { combineReducers } from 'redux';
import clientReducer from './client-reducer';

let reducers = combineReducers({
	clientState: clientReducer,
});

export default reducers;
