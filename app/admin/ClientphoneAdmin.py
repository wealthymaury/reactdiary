# coding=utf-8

from django.contrib import admin

from ..models import ClientPhone


class ClientPhoneInline(admin.TabularInline):
	model = ClientPhone
	extra = 1

@admin.register(ClientPhone)
class ClientPhoneAdmin(admin.ModelAdmin):
	list_per_page = 10
	