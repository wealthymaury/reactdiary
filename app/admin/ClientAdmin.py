# coding=utf-8

from django.contrib import admin

from django_extensions.admin import ForeignKeyAutocompleteAdmin

from ..models import Client
from .ClientphoneAdmin import ClientPhoneInline

@admin.register(Client)
class ClientAdmin(ForeignKeyAutocompleteAdmin):
	list_per_page = 10
	list_display = ('first_name', 'last_name', 'bank', 'account_number', 'clabe_formated_admin', 'card_number_formated_admin', 'timestamp',)
	list_display_links = ('first_name', 'last_name',)
	list_filter = ('bank',)
	search_fields = ('first_name', 'last_name', 'account_number', 'clabe', 'card_number',)
	inlines = (ClientPhoneInline, )
	fieldsets = (
		('Datos personales', {
			'fields': ('first_name','last_name','rfc',)
		}),
		('Datos domiciliarios', {
			'fields': ('address','num_ext','num_int','zip_code','colony','city','state','country')
		}),
		('Datos bancarios', {
			'fields': ('bank','account_number','clabe','card_number')
		}),
		('Datos adicionales', {
			'fields': ('user','description')
		}),
	)
	related_search_fields = {
		'user': ('username',)
	}

	def clabe_formated_admin(self, obj):
		return obj.get_clabe_display()

	clabe_formated_admin.admin_order_field = 'clabe'
	clabe_formated_admin.short_description = 'CLABE interbancaria'

	def card_number_formated_admin(self, obj):
		return obj.get_card_number_display()

	card_number_formated_admin.admin_order_field = 'card_number'
	card_number_formated_admin.short_description = 'Numero de tarjeta'
