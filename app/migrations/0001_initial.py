# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-13 23:58
from __future__ import unicode_literals

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=60, verbose_name='Nombre')),
                ('last_name', models.CharField(max_length=60, verbose_name='Apellidos')),
                ('rfc', models.CharField(blank=True, max_length=15, null=True, validators=[django.core.validators.MinLengthValidator(12)], verbose_name='RFC')),
                ('email', models.EmailField(blank=True, max_length=100, null=True, verbose_name='Correo electronico')),
                ('address', models.CharField(blank=True, max_length=40, null=True, verbose_name='Domicilio')),
                ('num_ext', models.CharField(blank=True, max_length=10, null=True, verbose_name='N\xfamero exterior')),
                ('num_int', models.CharField(blank=True, max_length=10, null=True, verbose_name='N\xfamero interior')),
                ('zip_code', models.CharField(blank=True, max_length=10, null=True, verbose_name='Codigo postal')),
                ('colony', models.CharField(blank=True, max_length=40, null=True, verbose_name='Colonia')),
                ('city', models.CharField(blank=True, max_length=40, null=True, verbose_name='Ciudad')),
                ('state', models.CharField(blank=True, max_length=40, null=True, verbose_name='Estado')),
                ('country', models.CharField(blank=True, max_length=40, null=True, verbose_name='Pais')),
                ('bank', models.CharField(blank=True, max_length=30, null=True, verbose_name='Banco')),
                ('account_number', models.CharField(blank=True, max_length=16, null=True, validators=[django.core.validators.MinLengthValidator(8)], verbose_name='Numero de cuenta')),
                ('clabe', models.CharField(blank=True, max_length=16, null=True, validators=[django.core.validators.MinLengthValidator(16)], verbose_name='Clabe interbancaria')),
                ('card_number', models.CharField(blank=True, max_length=16, null=True, validators=[django.core.validators.MinLengthValidator(16)], verbose_name='Numero de tarjeta')),
                ('description', models.TextField(blank=True, max_length=140, null=True, verbose_name='descripcion')),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de registro')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Usuario')),
            ],
            options={
                'ordering': ['last_name', 'first_name'],
                'verbose_name': 'Cliente',
                'verbose_name_plural': 'Clientes',
                'permissions': (('view_client', 'Can see client'),),
            },
        ),
        migrations.CreateModel(
            name='ClientPhone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=10, unique=True, validators=[django.core.validators.MinLengthValidator(10)], verbose_name='N\xfamero de tel\xe9fono')),
                ('type', models.CharField(choices=[('cel', 'Celular'), ('tel', 'Fijo'), ('nex', 'Nextel')], max_length=3, verbose_name='Tipo de tel\xe9fono')),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='Fecha de registro')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Client', verbose_name='Cliente')),
            ],
            options={
                'ordering': ['timestamp', 'client'],
                'verbose_name': 'Tel\xe9fono de cliente',
                'verbose_name_plural': 'Tel\xe9fonos del cliente',
                'permissions': (('view_clientphone', 'Can see clientphone'),),
            },
        ),
        migrations.AlterUniqueTogether(
            name='client',
            unique_together=set([('first_name', 'last_name')]),
        ),
    ]
