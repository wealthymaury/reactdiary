from django.db.models import Q

from rest_framework import viewsets


from ..models import Client
from ..serializers import ClientSerializer

class ClientViewset(viewsets.ModelViewSet):
	model = Client
	queryset = Client.objects.all()
	serializer_class = ClientSerializer
