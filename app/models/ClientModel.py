# coding=utf-8

from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator


class Client(models.Model):
	"""
	Description: Modelo de un cliente
	"""

	first_name = models.CharField(max_length=60, verbose_name='Nombre')
	last_name = models.CharField(max_length=60, verbose_name='Apellidos')
	user = models.ForeignKey(User, verbose_name='Usuario', blank=True, null=True)
	rfc = models.CharField(max_length=15, validators=[ MinLengthValidator(12) ], null=True, blank=True, verbose_name='RFC')
	email = models.EmailField(max_length=100, null=True, blank=True, verbose_name='Correo electronico')
	address = models.CharField(max_length=40, null=True, blank=True, verbose_name='Domicilio')
	num_ext = models.CharField(max_length=10, verbose_name='Número exterior', null=True, blank=True,)
	num_int = models.CharField(max_length=10, verbose_name='Número interior', null=True, blank=True)
	zip_code = models.CharField(max_length=10, null=True, blank=True, verbose_name='Codigo postal')
	colony = models.CharField(max_length=40, null=True, blank=True, verbose_name='Colonia')
	city = models.CharField(max_length=40, null=True, blank=True, verbose_name='Ciudad')
	state = models.CharField(max_length=40, null=True, blank=True, verbose_name='Estado')
	country = models.CharField(max_length=40, null=True, blank=True, verbose_name='Pais')
	bank = models.CharField(max_length=30, null=True, blank=True, verbose_name='Banco')
	account_number = models.CharField(max_length=16, null=True, blank=True, validators=[ MinLengthValidator(8) ], verbose_name='Numero de cuenta')
	clabe = models.CharField(max_length=16, null=True, blank=True, validators=[ MinLengthValidator(16) ], verbose_name='Clabe interbancaria')
	card_number = models.CharField(max_length=16, null=True, blank=True, validators=[ MinLengthValidator(16) ], verbose_name='Numero de tarjeta')
	description = models.TextField(max_length=140, null=True, blank=True, verbose_name='descripcion')
	timestamp = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de registro')

	class Meta:
		verbose_name = 'Cliente'
		verbose_name_plural = 'Clientes'
		ordering = ['last_name', 'first_name']
		unique_together = (("first_name", "last_name"),)
		permissions = (
			("view_client", "Can see client"),
		)

	def __str__(self):
		return self.full_name()

	def __unicode__(self):
		return self.full_name()

	def full_name(self):
		return "{first_name} {last_name}".format(
			first_name=self.first_name,
			last_name=self.last_name
		)

	def get_clabe_display(self):
		string = self.clabe
		formated = "%s %s %s %s" % (string[0:4], string[4:8], string[8:12], string[12:16])

		return formated

	def get_card_number_display(self):
		string = self.card_number
		formated = "%s %s %s %s" % (string[0:4], string[4:8], string[8:12], string[12:16])
		return formated

	def phones(self):
		return self.clientphone_set.select_related()
