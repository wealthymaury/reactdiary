# coding=utf-8

from __future__ import unicode_literals

from django.db import models
from django.core.validators import MinLengthValidator

from .ClientModel import Client


class ClientPhone(models.Model):
	"""
	Description: Modelo de telefono de cliente
	"""

	_PHONE_TYPES = (
		('cel', 'Celular'),
		('tel', 'Fijo'),
		('nex', 'Nextel'),
	)

	client = models.ForeignKey(Client, verbose_name='Cliente')
	number = models.CharField(max_length=10, unique=True, validators=[ MinLengthValidator(10) ], verbose_name='Número de teléfono')
	type = models.CharField(max_length=3, choices=_PHONE_TYPES, verbose_name='Tipo de teléfono')
	timestamp = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de registro')

	class Meta:
		verbose_name = 'Teléfono de cliente'
		verbose_name_plural = 'Teléfonos de clientes'
		ordering = ['timestamp', 'client']
		permissions = (
			("view_clientphone", "Can see clientphone"),
		)

	def __str__(self):
		return self.get_phone_display()

	def __unicode__(self):
		return self.get_phone_display()

	def get_phone_display(self):
		return ("(%s%s%s) %s%s%s-%s%s%s%s" % tuple(self.number))
		