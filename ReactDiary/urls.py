from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import routers

from app.viewsets import ClientViewset
from app.views import IndexTemplateView

router = routers.DefaultRouter()
router.register(r'clients', ClientViewset)

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^api/', include(router.urls)),
	url(r'^', IndexTemplateView.as_view(), name='home')
]
